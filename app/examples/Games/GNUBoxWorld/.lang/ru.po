# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 07:08+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Games/GNUBoxWorld/.project:20
msgid "GNUBoxWorld"
msgstr "Мир GNU-блоков"

#: app/examples/Games/GNUBoxWorld/.project:21
msgid ""
"This is another version of the popular game Box World.\n"
"\n"
"It contains 16 levels, all are possible to resolve. Level designs are taken from the Box It game for mobile phones."
msgstr ""
"Это ещё одна версия популярной игры Мира блоков.\n"
"\n"
"Она содержит 16 уровней, все можно решить. Дизайн уровней взят из игры «Box It/Заблокуй» для мобильных телефонов."

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:42
msgid "Level &1"
msgstr "Уровень &1"

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:81
msgid "GNUBoxWorld - Congratulation! You're very clever!"
msgstr "Мир GNU-блоков - Поздравляем! Вы очень умные!"

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:83
msgid "Next level"
msgstr "Следующий уровень"

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:83
msgid "Good Luck!"
msgstr "Удачи!"

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:223
msgid "GNUBoxWorld - Level "
msgstr "Мир GNU-блоков - Уровень "

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:255
msgid "Are you sure?"
msgstr "Вы уверены?"

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:255
msgid "Yes"
msgstr "Да"

#: app/examples/Games/GNUBoxWorld/.src/FMain.class:255
msgid "No"
msgstr "Нет"

#: app/examples/Games/GNUBoxWorld/.src/FMain.form:8
msgid "Game"
msgstr "Игра"

#: app/examples/Games/GNUBoxWorld/.src/FMain.form:10 app/examples/Games/GNUBoxWorld/.src/FMain.form:35
msgid "Clear"
msgstr "Очистить"

#: app/examples/Games/GNUBoxWorld/.src/FMain.form:14
msgid "Quit"
msgstr "Выход"

#: app/examples/Games/GNUBoxWorld/.src/FMain.form:19
msgid "Level"
msgstr "Уровень"

#: app/examples/Games/GNUBoxWorld/.src/FMain.form:22
msgid "Help"
msgstr "Справка"

#: app/examples/Games/GNUBoxWorld/.src/FMain.form:24 app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:28
msgid "How to play?"
msgstr "Как играть?"

#: app/examples/Games/GNUBoxWorld/.src/FMain.form:28
msgid "About"
msgstr "О программе"

#: app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:5
msgid "About..."
msgstr "О программе..."

#: app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:12
msgid "Author"
msgstr "Автор"

#: app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:15
msgid ""
"Author: Pablo Mileti.<br> <br>You can submit your questions, suggestions, bugs, etc, to the following E-Mail address:\n"
"<a href=\"mailto:pablomileti@gmail.com\"> pablomileti@gmail.com</a>"
msgstr ""
"Автор: Пабло Милети.<br><br>Вы можете отправить свои вопросы, предложения, ошибки и т. д. на следующий электронный адрес:\n"
"<a href=\"mailto:pablomileti@gmail.com\">pablomileti@gmail.com</a>"

#: app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:19
msgid "License"
msgstr "Лицензия"

#: app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:22
msgid ""
"\n"
"    Copyright (C) 2010. Author: Pablo Mileti  \n"
"\n"
"This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>."
msgstr ""
"\n"
"    Copyright (C) 2010. Автор: Пабло Милети\n"
"\n"
"Эта программа ― свободное программное обеспечение. Вы можете распространять или изменять его при условиях соответствия лицензии GNU General Public License опубликованной Free Software Foundation; либо версии 3 лицензии, либо (на ваше усмотрение) любой более поздней версии.\n"
"\n"
"Программа распространяется в надежде на то, что приложение будет полезно, но БЕЗ ВСЯКИХ ГАРАНТИЙ; не гарантируется даже ПРИГОДНОСТЬ или СООТВЕТСТВИЕ КАКИМ-ЛИБО ТРЕБОВАНИЯМ. Для получения дополнительной информации ознакомьтесь с лицензией GNU General Public License.\n"
"\n"
"Вы должны получить копию лицензии GNU General Public License вместе с программой. Если этого не произошло, посмотрите <http://www.gnu.org/licenses/>."

#: app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:31
msgid ""
"<b>Aim</b><br>\n"
"The shrimp has to push the movable boxes to the marked positions.<br><br>\n"
"<b>Move</b><br>\n"
"To move around the game board, use the arrow keys: up, down, left and right.<br><br>\n"
"<b>Clear</b><br>\n"
"To restart the level, press the spacebar key."
msgstr ""
"<b>Цель</b><br>\n"
"Креветка должна толкать подвижные блоки в отмеченные позиции.<br><br>\n"
"<b>Перемещение</b><br>\n"
"Для перемещения по игровому полю используйте клавиши со стрелками: вверх, вниз, влево и вправо.\n"
"<b>Очистить</b><br>\n"
"Чтобы перезапустить уровень, нажмите клавишу пробела."

#: app/examples/Games/GNUBoxWorld/.src/FrmAbout.form:38
msgid "&Close"
msgstr "Закрыть"

